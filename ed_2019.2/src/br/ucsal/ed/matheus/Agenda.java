package br.ucsal.ed.matheus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Agenda {
	private static SimpleDateFormat formatdata = new SimpleDateFormat("dd/MM/yyyy");
	private static boolean lotada = false;
	private Contato[] contatos;

	public Agenda() {

		this.contatos = new Contato[5];

	}

	public void inserir(Contato contato) {
		int localiza = verificar();

		if (lotada)
			return;
		this.contatos[localiza] = contato;
		System.out.println("Posi��o adicionada com sucesso");

	}

	public int verificar() {

		Boolean cheia = false;
		if (this.contatos.length == 5) {
			for (int i = 0; i < 5; i++) {
				System.out.println("Posi��o " + i + " : " + this.contatos[i]);
				if (this.contatos[i] == null) {
					return i;
				} else {
					cheia = true;
				}
			}
			if (cheia && this.contatos.length == 5) {

				Contato newList[] = new Contato[10];
				for (int a = 0; a < 5; a++) {
					newList[a] = this.contatos[a];
				}
				this.contatos = newList;
				System.out.println("Lista duplicada");
				return 6;
			}
		}
		for (int i = 0; i < 10; i++) {
			System.out.println("Posi��o " + i + " : " + this.contatos[i]);
			if (this.contatos[i] == null) {
				return i;
			} else {
				cheia = true;
			}
		}
		if (cheia) {
			lotada = true;
			System.out.println("Lista cheia.");
		}

		return 6;
	}

	public void remover(String cpf) {
		for (int i = 0; i < this.contatos.length; i++) {
			if (this.contatos[i] != null && this.contatos[i].getCpf().equals(cpf)) {
				System.out.println("Contato sendo removido: " + this.contatos[i].getNome() + " . CPF: "
						+ this.contatos[i].getCpf());
				this.contatos[i] = new Contato();
				System.out.println("Contato " + contatos[i].getNome() + contatos[i].getCpf() + " removido!");

			}
		}
	}

	Contato[] consultarPorBairro(String bairro) {

		Contato listaContatos[] = new Contato[10];
		for (int i = 0; i < this.contatos.length; i++) {
			if (this.contatos[i].getBairro() == bairro) {
				for (int a = 0; a < 10; a++) {
					if (listaContatos[a] == null) {
						listaContatos[a] = this.contatos[i];
					}
				}
			}
		}

		return listaContatos;
	}

	public Contato consultarPorCpf(String cpf) {

		for (int i = 0; i < this.contatos.length; i++) {
			if (this.contatos[i] != null && this.contatos[i].getCpf().equals(cpf)) {
				System.out.println("Contato retornado: " + this.contatos[i].getNome());
				return this.contatos[i];
			}
		}
		return null;

	}

	public Contato[] consultarPorCidade(String cidade) {
		int aux = 0;
		Contato listaContatos[] = new Contato[this.contatos.length];
		for (int i = 0; i < this.contatos.length; i++) {
			if (this.contatos[i] != null && this.contatos[i].getCidade().equals(cidade)) {
				listaContatos[aux] = this.contatos[i];
				aux++;
			}
		}

		System.out.println("Lista de contatos da cidade: " + cidade);
		for (Contato contato : listaContatos) {
			if (contato != null && contato.getNome() != null)
				System.out.println("Contato: " + contato.getNome());
		}
		return listaContatos;

	}

	public void atualizar(Contato contato) {
		Scanner sc = new Scanner(System.in);
		Contato novoContato = new Contato();
		
		System.out.println("Insira o nome: " + contato.getNome());
		novoContato.setNome(sc.next());
		System.out.println("Insira o cpf: " + contato.getCpf());
		novoContato.setCpf(sc.next());
		System.out.println("Insira o email: " + contato.getEmail());
		novoContato.setEmail(sc.next());
		System.out.println("Insira a cidade: " + contato.getCidade());
		novoContato.setCidade(sc.next());
		System.out.println("Insira o logradouro: " + contato.getLogradouro());
		novoContato.setLogradouro(sc.next());
		System.out.println("Insira o bairro: " + contato.getBairro());
		novoContato.setBairro(sc.next());
		System.out.println("Insira o n�mero: " + contato.getNumero());
		novoContato.setNumero(sc.nextInt());
		System.out.println("Insira a data:  " + contato.getDataDeNascimento());

		try {
			novoContato.setDataDeNascimento(formatdata.parse(sc.next()));
		} catch (ParseException e) {

			e.printStackTrace();
		}

		for (int i = 0; i < 10; i++) {
			if (this.contatos[i] != null && this.contatos[i].equals(contato)) {
				this.contatos[i] = novoContato;
				System.out.println("Contato atualizado!");
				return;
			}
		}

	}

	public void imprimirContato(Contato contato) {
		System.out.println("Contato: " + contato.getCpf());

	}

	public void imprimirTodosContatos() {

		for (Contato contato : this.contatos) {
			if (contato != null && contato.getNome() != null && contato.getCpf() != null && contato.getCidade() != null)
				System.out.println("Contato: " + contato.getNome() + " . CPF: " + contato.getCpf() + ". Cidade: "
						+ contato.getCidade());
		}

	}

	public void ordenaPorCpf(boolean reverse) {
		System.out.println("A posi��o escolhida foi: " + reverse);
		String cpfs[] = new String[this.contatos.length];
		int cpfAux = 0;
		int pos;
		String temp;

		System.out.println("Ordenando" + cpfs.length + " elementos");

		for (int i = 0; i < this.contatos.length; i++) {
			if (this.contatos[i] != null && this.contatos[i].getCpf() != null) {
				cpfs[cpfAux] = this.contatos[i].getCpf();
				cpfAux++;
			}
		}
		int auxCpfs = 0;
		System.out.println("Lista de cpfs:");
		for (String cpf : cpfs) {
			if (cpf != null)
				System.out.println(cpf);
			auxCpfs++;
		}

		if (reverse == false) {

			for (int it = 0; it < cpfs.length - 1; it++) {
				for (pos = 0; pos < cpfs.length - 1; pos++) {

					if ((cpfs[pos + 1].compareTo(cpfs[pos]) < 0)) {

						temp = cpfs[pos];
						cpfs[pos] = cpfs[pos + 1];
						cpfs[pos + 1] = temp;
					}

				}
			}
			System.out.println("Lista atualizada:");
			for (String cpf : cpfs) {
				if (cpf != null)
					System.out.println(cpf);
			}

		} else {

			for (int it = 0; it < cpfs.length - 1; it++) {
				for (pos = 0; pos < cpfs.length - it - 1; pos++) {

					if ((cpfs[pos + 1].compareTo(cpfs[pos]) > 0)) {

						temp = cpfs[pos];
						cpfs[pos] = cpfs[pos + 1];
						cpfs[pos + 1] = temp;
					}

				}
			}
			System.out.println("Lista atualizada:");
			for (String cpf : cpfs) {
				if (cpf != null)
					System.out.println(cpf);
			}
		}

	}

	public void ordenaPorNome(boolean reverse) {

		System.out.println("A posi��o escolhida foi: " + reverse);
		String nomes[] = new String[this.contatos.length];
		int nomesAux = 0;
		int pos;
		String temp;

		System.out.println("Ordenando " + nomes.length + " elementos");

		for (int i = 0; i < this.contatos.length; i++) {
			if (this.contatos[i] != null && this.contatos[i].getNome() != null) {
				nomes[nomesAux] = this.contatos[i].getNome();
				nomesAux++;
			}
		}
		int auxNomes = 0;
		System.out.println("Lista de nomes:");
		for (String nome : nomes) {
			if (nome != null)
				System.out.println(nome);
			auxNomes++;
		}
		if (reverse == false) {

			for (int it = 0; it < nomes.length - 1; it++) {
				for (pos = 0; pos < nomes.length - 1; pos++) {

					if ((nomes[pos + 1].compareTo(nomes[pos]) < 0)) {

						temp = nomes[pos];
						nomes[pos] = nomes[pos + 1];
						nomes[pos + 1] = temp;
					}

				}
			}
			System.out.println("Lista atualizada:");
			for (String nome : nomes) {
				if (nome != null)
					System.out.println(nomes);
			}

		} else {

			for (int it = 0; it < nomes.length - 1; it++) {
				for (pos = 0; pos < nomes.length - it - 1; pos++) {

					if ((nomes[pos + 1].compareTo(nomes[pos]) > 0)) {

						temp = nomes[pos];
						nomes[pos] = nomes[pos + 1];
						nomes[pos + 1] = temp;
					}

				}
			}
			System.out.println("Lista atualizada:");
			for (String nome : nomes) {
				if (nome != null)
					System.out.println(nome);
			}
		}

	}

	public void ordenaPorDataDeNascimento(boolean reverse) {
		SimpleDateFormat formatdata = new SimpleDateFormat("dd/MM/yyyy");
		System.out.println("A opo escolhida foi: " + reverse);

		Date datasDeNascimento[] = new Date[this.contatos.length];
		int dataDeNascimentoAux = 0;
		int pos;
		Date temp;

		System.out
				.println("Realizando Ordenao por BubbleSort em um vetor de " + datasDeNascimento.length + " elementos");

		for (int i = 0; i < this.contatos.length; i++) {
			if (this.contatos[i] != null && this.contatos[i].getDataDeNascimento() != null) {
				datasDeNascimento[dataDeNascimentoAux] = this.contatos[i].getDataDeNascimento();
				dataDeNascimentoAux++;
			}
		}
		int auxDataDeNascimento = 0;
		System.out.println("Lista de datas:");
		for (Date data : datasDeNascimento) {
			if (data != null)
				System.out.println(data);
			auxDataDeNascimento++;
		}

		if (reverse == false) {

			for (int it = 0; it < datasDeNascimento.length - 1; it++) {
				for (pos = 0; pos < datasDeNascimento.length - 1; pos++) {

					if ((datasDeNascimento[pos + 1].compareTo(datasDeNascimento[pos]) < 0)) {

						temp = datasDeNascimento[pos];
						datasDeNascimento[pos] = datasDeNascimento[pos + 1];
						datasDeNascimento[pos + 1] = temp;
					}

				}
			}
			System.out.println("Lista atualizada:");
			for (Date datas : datasDeNascimento) {
				if (datas != null)
					System.out.println(datas);
			}

		} else {

			for (int it = 0; it < datasDeNascimento.length - 1; it++) {
				for (pos = 0; pos < datasDeNascimento.length - it - 1; pos++) {

					if ((datasDeNascimento[pos + 1].compareTo(datasDeNascimento[pos]) > 0)) {

						temp = datasDeNascimento[pos];
						datasDeNascimento[pos] = datasDeNascimento[pos + 1];
						datasDeNascimento[pos + 1] = temp;
					}

				}
			}

			System.out.println("Lista atualizada:");
			for (Date data : datasDeNascimento) {
				if (data != null)
					System.out.println(data);
			}
		}
	}

}
