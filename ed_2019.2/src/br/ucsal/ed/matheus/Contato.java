package br.ucsal.ed.matheus;

	import java.text.ParseException;
	import java.text.SimpleDateFormat;
	import java.util.Date;

	public class Contato implements IContato {

		private SimpleDateFormat formatdata = new SimpleDateFormat("dd/MM/yyyy");
		
		private String cpf;
		private String nome;
		private String email;
		private String telefone;
		private String logradouro;
		private int numero;
		private String bairro;
		private String cidade;
		private Date dataDeNascimento;

		

		public Contato() {

		}

		public Contato(String cpf, String nome, String email, String telefone, String logradouro, int numero, String bairro,
				String cidade, String dataDeNascimento) {

			this.cpf = cpf;
			this.nome = nome;
			this.email = email;
			this.telefone = telefone;
			this.logradouro = logradouro;
			this.numero = numero;
			this.bairro = bairro;
			this.cidade = cidade;

			try {
				this.dataDeNascimento = formatdata.parse(dataDeNascimento);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}

		@Override
		public void setCpf(String cpf) {
			this.cpf = cpf;

		}

		@Override
		public String getCpf() {

			return cpf;
		}

		@Override
		public void setNome(String nome) {
			this.nome = nome;

		}

		@Override
		public String getNome() {

			return nome;
		}

		@Override
		public void setEmail(String email) {
			this.email = email;

		}

		@Override
		public String getEmail() {

			return email;
		}

		@Override
		public void setTelefone(String telefone) {
			this.telefone = telefone;

		}

		@Override
		public String getTelefone() {

			return telefone;
		}

		@Override
		public void setLogradouro(String logradouro) {
			this.logradouro = logradouro;

		}

		@Override
		public String getLogradouro() {

			return logradouro;
		}

		@Override
		public void setNumero(int numero) {
			this.numero = numero;

		}

		@Override
		public int getNumero() {

			return numero;

		}

		@Override
		public void setBairro(String bairro) {
			this.bairro = bairro;

		}

		@Override
		public String getBairro() {

			return bairro;
		}

		@Override
		public void setCidade(String cidade) {
			this.cidade = cidade;

		}

		@Override
		public String getCidade() {

			return cidade;
		}

		@Override
		public void setDataDeNascimento(Date data_nascimento) {
			this.dataDeNascimento = data_nascimento;

		}

		@Override
		public Date getDataDeNascimento() {

			return dataDeNascimento;
		}

	}


