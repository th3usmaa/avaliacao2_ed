package br.ucsal.ed.matheus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class AgendaContato {

	private static SimpleDateFormat formatdata = new SimpleDateFormat("dd/MM/yyyy");
	Contato contato2;
	private static int num = 1;

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		Agenda agenda = new Agenda();
		int dec;

		do {
			System.out.println(
					"|Menu Agenda|\n\n| 1 - Inserir contato | 2 - Remover \n\n| 3 - Consultar por CPF | 4 - Consultar por Cidade \n\n| 5 - Atualizar contato "
							+ "\n\n| 6 - Imprimir contato por CPF | 7 - Imprimir todos contatos \n\n| 8 - Ordenar por CPF | 9 - Ordenar por nome "
							+ "| 10 - Ordenar por data de nascimento \n\n| 11 - Sair");

			dec = scan.nextInt();

			switch (dec) {
			case 1:
				Contato contato2 = criarContato(scan);
				agenda.inserir(contato2);
				break;
			case 2:
				agenda.imprimirTodosContatos();
				System.out.println("Digite o CPF do contato que voc� deseja remover: ");
				agenda.remover(scan.next());
				break;
			case 3:
				System.out.println("Digite o CPF do contato que voc� deseja buscar: ");
				agenda.imprimirTodosContatos();
				agenda.consultarPorCpf(scan.next());
				break;
			case 4:
				System.out.println("Digite a cidade do contato que voc� deseja buscar:");
				agenda.imprimirTodosContatos();
				agenda.consultarPorCidade(scan.next());
				break;
			case 5:
				agenda.imprimirTodosContatos();
				System.out.println("Digite o CPF do contato a ser atualizado");
				String cpf = scan.nextLine();
				Contato contatoAntigo = agenda.consultarPorCpf(cpf);
				agenda.atualizar(contatoAntigo);
				break;
			case 6:
				agenda.imprimirTodosContatos();
				System.out.println("Digite o CPF do contato a ser impresso");
				String cppf = scan.next();
				Contato contato = agenda.consultarPorCpf(cppf);
				agenda.imprimirContato(contato);
				break;
			case 7:
				agenda.imprimirTodosContatos();
				break;
			case 8:
				System.out.println("true - ordena��o decrescente | false - ordena��o crescente.");
				agenda.ordenaPorCpf(scan.nextBoolean());
				break;
			case 9:
				System.out.println("true - ordena��o decrescente | false - ordena��o crescente.");
				agenda.ordenaPorNome(scan.nextBoolean());
				break;
			case 10:
				System.out.println("true - ordena��o decrescente | false - ordena��o crescente.");
				agenda.ordenaPorDataDeNascimento(scan.nextBoolean());
				break;

			case 11:
				System.out.println("Bye.");
				return;

			default:
				System.out.println("Op��o Inv�lida! Tente novamente.");
			}

		} while (dec != 11);

	}

	private static Contato criarContato(Scanner scan) {
		Contato contato2 = new Contato();

		System.out.println("Digite o CPF: ");
		contato2.setCpf(scan.next());

		System.out.println("Digite o NOME: ");
		contato2.setNome(scan.next());

		System.out.println("Digite o EMAIL: ");
		contato2.setEmail(scan.next());

		System.out.println("Digite o TELEFONE: ");
		contato2.setTelefone(scan.next());

		System.out.println("Digite o LOGRADOURO: ");
		contato2.setLogradouro(scan.next());

		System.out.println("Digite o N�MERO: ");
		contato2.setNumero(scan.nextInt());

		System.out.println("Digite o BAIRRO: ");
		contato2.setBairro(scan.next());

		System.out.println("Digite a CIDADE: ");
		contato2.setCidade(scan.next());

		try {

			System.out.println("Digite a DATA DE NASCIMENTO - dd/MM/yyyy: ");
			contato2.setDataDeNascimento(formatdata.parse(scan.next()));
		} catch (ParseException e) {

			e.printStackTrace();
		}
		return contato2;
	}

}
